
This sets up a Linux VM (ChromeBook or not) with basic utilities data engineering/science tools.

## Tested Configurations
- aarch64: Ubuntu 20.04
- amd64: Ubuntu 20.04, Debian 10.7, Debian 11.7
- armv7l: Debian 10.6

# After you run the prep.sh

```
~/.local/bin/ansible-playbook bootstrap.yml
```
